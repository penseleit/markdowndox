var vscode = require('vscode');
var config_worker = require('./config_worker');
var fs = require('fs');
var path = require('path');

var pdfChromeExecutablePath = '';

function getPDFOptions(fileUri)
{
    var pdfOutputFileName = '';
    var pdfScale = 1;
    var pdfPaperFormat = '';
    var pdfDisplayHeaderFooter = true;
    var pdfHeaderTemplate = '';
    var pdfFooterTemplate = '';
    var pdfPrintBackgroundGraphics = true;
    var pdfOrientationLandscape = false;
    var pdfPageRanges = '';
    var pdfWidth = '';
    var pdfHeight = '';
    var pdfMarginTop = '';
    var pdfMarginRight = '';
    var pdfMarginBottom = '';
    var pdfMarginLeft = '';
    var pdfPreferCSSPageSize = '';
    var pdfOutputFilePath = '';

    try{
        var configJSON = config_worker.getConfigJSON(fileUri);
        
        pdfScale = configJSON.pdfScale;
        pdfPaperFormat = configJSON.pdfPaperFormat;
        pdfDisplayHeaderFooter = configJSON.pdfDisplayHeaderFooter;
        pdfHeaderTemplate = configJSON.pdfHeaderTemplate;
        pdfFooterTemplate = configJSON.pdfFooterTemplate;
        pdfPrintBackgroundGraphics = configJSON.pdfPrintBackgroundGraphics;
        pdfOrientationLandscape = configJSON.pdfOrientationLandscape;
        pdfPageRanges = configJSON.pdfPageRanges;
        pdfWidth = configJSON.pdfWidth;
        pdfHeight = configJSON.pdfHeight;
        pdfMarginTop = configJSON.pdfMarginTop;
        pdfMarginRight = configJSON.pdfMarginRight;
        pdfMarginBottom = configJSON.pdfMarginBottom;
        pdfMarginLeft = configJSON.pdfMarginLeft;
        pdfPreferCSSPageSize = configJSON.pdfPreferCSSPageSize;
        pdfChromeExecutablePath = configJSON.pdfChromeExecutablePath;

        //Need to set top and bottom margins if header and/or footer displayed
        if (pdfDisplayHeaderFooter == true){
            if(! pdfMarginTop){
                pdfMarginTop = '1cm';
            }
            if(! pdfMarginBottom){
                pdfMarginBottom = '1cm';
            }
        }
        //If pdfHeaderTemplate value is an empty string then default header will display in the output pdf, so use space character to create an empty header in the output pdf.
        if(pdfHeaderTemplate == ''){
            pdfHeaderTemplate = ' '; 
        }

        if(pdfChromeExecutablePath){
            pdfChromeExecutablePath = path.normalize(pdfChromeExecutablePath);
        }
        
        pdfOutputFileName = path.basename(fileUri.fsPath, '.html') + '.pdf';
    }
    catch(e){
        console.error('::pdf_generator:: Error reading configuration values for PDF generation. ' + e.message);
    } 

    //Get output file path
    var projOutFolder  = configJSON.project_output_folder;
    var rootFolder = config_worker.getRootFolder(fileUri);
    if(! rootFolder){
        pdfOutputFilePath = path.join(path.dirname(fileUri.fsPath), path.sep, pdfOutputFileName); //Fallback to same folder as source HTML
    }
    else{
        pdfOutputFilePath = path.join(rootFolder, path.sep, projOutFolder, path.sep, pdfOutputFileName);
    }
    

    var options = {
        'path':pdfOutputFilePath, 
        'scale':pdfScale,
        'displayHeaderFooter':pdfDisplayHeaderFooter,
        'headerTemplate':pdfHeaderTemplate,
        'footerTemplate':pdfFooterTemplate,
        'printBackground':pdfPrintBackgroundGraphics,
        'landscape':pdfOrientationLandscape,
        'pageRanges':pdfPageRanges,
        'format':pdfPaperFormat,
        'width':pdfWidth,
        'height':pdfHeight,
        'margin':{'top':pdfMarginTop,'right':pdfMarginRight,'bottom':pdfMarginBottom,'left':pdfMarginLeft},
        'preferCSSPageSize':pdfPreferCSSPageSize
    };

    return options;
}

module.exports = {

    //Create pdf from Markdown file
    createFilePDF: function(fileUri) {  

        var htmlURI = '';
        var htmlPath = '';

         // Process MD file into HTML
         if (! fileUri){
             return;
         }
      
        try{
            
            //Check file exists
            fs.accessSync(fileUri.fsPath, fs.constants.F_OK | fs.constants.R_OK);

            //Read html
            htmlPath = path.normalize(fileUri.fsPath);

            if (htmlPath.indexOf('.html') == -1){  //Make sure selected file is HTML - IMPORTANT
                throw 'Selected file is not a .html file';
            }
            htmlURI = vscode.Uri.file(htmlPath);
        }
        catch(e){
            console.error('::pdf_generator:: Error reading HTML file for PDF generation. ' + e.message);
            return;
        }

        if (htmlURI){
            
            var pdfOptions = getPDFOptions(fileUri);
            
            if (pdfOptions){

                const puppeteer = require('puppeteer');
                
                vscode.window.withProgress({
                    location: vscode.ProgressLocation.Notification, //Show in Notification Panel
                    title: 'Generating PDF file',
                    cancellable: false
                }, async function generatePDF (progress, token) {

                    var browser = null;

                    try {
                        console.log('Generating PDF file from "' + path.basename(htmlURI.fsPath) + '".');
                        
                        progress.report({message: 'Initializing ...', increment: 10});
                        if (pdfChromeExecutablePath)
                        {
                            browser = await puppeteer.launch({executablePath: pdfChromeExecutablePath});
                        }
                        else{
                            browser = await puppeteer.launch(); 
                        }
                    
                        progress.report({message: 'Loading HTML file ...', increment: 20})
                        const page = await browser.newPage();
                        await page.goto(htmlURI.toString(),{waitUntil: 'networkidle0'});

                        progress.report({message: 'Writing PDF ...', increment: 30})
                        var pdfResult = await page.pdf(pdfOptions);

                        progress.report({message: 'Finishing up...', increment: 90})
                        await browser.close();

                        if (pdfResult.length > 0){
                            console.log('File "' + path.basename(htmlURI.fsPath) + '" has been generated as "' + path.basename(pdfOptions.path) + '".');
                            vscode.window.showInformationMessage('File "' + path.basename(htmlURI.fsPath) + '" has been generated as "' + path.basename(pdfOptions.path) + '".');
                        }
                                            
                    }
                    catch(e){
                        console.error('::pdf_generator:: Error processing PDF file. ' + e.message);
                        vscode.window.showErrorMessage('Error processing PDF file. ' + e.message);
                        
                        if (browser){
                            await browser.close();
                        }
                    }
                });
            }
        }
    }
};