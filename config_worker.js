var vscode = require('vscode');
var fs = require('fs');
var path = require('path');

var configFileName = '/mdoxconfig.json';

//Set defaults in internal configuration JSON object
var defaultConfigurationJSONObject = {
    "project_name": "My MarkdownDox Project",
    "images_folder": "./images",
    "stylesheet_folder": "./css",
    "includes_folder": "./includes",
    "project_output_folder": "./docs",
    "stylesheet_path": "./css/mdox_default.css",
    "highlight_stylesheet_path": "./css/default.css",
    "use_internal_stylesheet": false,
    "insert_page_breaks": true,
    "project_files_list": [
        "index.md",
        "GettingStarted.md",
        "Sample.md",
        "CreatingCustomStylesheets.md"    
    ],
    "show_errors_in_output_html": true,
    "auto_include_header_footer_nav": true,
    "auto_generate_html_on_file_change": true,
    "auto_generate_html_on_include_file_change": true,
    "markdown_it_toc_settings":{
        "includeLevel": [2, 3, 4],
        "containerClass": "table-of-contents",
        "slugify": "(s) => encodeURIComponent(String(s).trim().toLowerCase().replace(/\s+/g, '-'))",
        "markerPattern": "/^\\[\\[toc\\]\\]/im",
        "listType": "ul",
        "format": "",
        "forceFullToc": false,
        "containerHeaderHtml": "",
        "containerFooterHtml": ""
    },
    "markdown_it_anchor_settings": {
        "level": 1,
        "slugify": "",
        "permalink": false,
        "renderPermalink": "",
        "permalinkClass": "header-anchor",
        "permalinkSymbol": "¶",
        "permalinkBefore": false,
        "permalinkHref": "",
        "callback": ""
    },
    "markdown_it_checkbox_settings": {
        "divWrap": true,
        "divClass": "cb",
        "idPrefix": "cbx_"
    },
    "markdown_it_emoji_settings": {
        "defs": "",
        "enabled": "",
        "shortcuts": ""

    },
    "pdfChromeExecutablePath": "",
    "pdfProjectFilesList": [
        "index.md",
        "GettingStarted.md",
        "Sample.md",
        "CreatingCustomStylesheets.md"    
    ],
    "pdfScale": 1,
    "pdfPaperFormat": "A4",
    "pdfDisplayHeaderFooter": true,
    "pdfHeaderTemplate": " ",
    "pdfFooterTemplate": "<div style=\"width: 100%; font-family: Arial; font-size: 9px; margin 0 auto; text-align: center \"><span style=\"margin-left: 20px;\">Copyright &copy; 2019 My Company</span> Page <span class='pageNumber'></span> of <span class='totalPages'></span></div>",
    "pdfPrintBackgroundGraphics": true,
    "pdfOrientationLandscape": false,
    "pdfPageRanges": "",
    "pdfWidth": "",
    "pdfHeight": "",
    "pdfMarginTop": "1cm",
    "pdfMarginRight": "",
    "pdfMarginBottom": "1cm",
    "pdfMarginLeft": "",
    "pdfPreferCSSPageSize": false
};

function updateConfigurationValuesFromFile(defaultConfigJSON, configJSONFromFile){
    
    if(configJSONFromFile.project_name != null){
        defaultConfigJSON.project_name = configJSONFromFile.project_name;
    }

    if(configJSONFromFile.images_folder != null){
        defaultConfigJSON.images_folder = configJSONFromFile.images_folder;
    }

    if(configJSONFromFile.stylesheet_folder != null){
        defaultConfigJSON.stylesheet_folder = configJSONFromFile.stylesheet_folder;
    }

    if(configJSONFromFile.includes_folder != null){
        defaultConfigJSON.includes_folder = configJSONFromFile.includes_folder;
    }

    if(configJSONFromFile.project_output_folder != null){
        defaultConfigJSON.project_output_folder = configJSONFromFile.project_output_folder;
    }

    if(configJSONFromFile.stylesheet_path != null){
        defaultConfigJSON.stylesheet_path = configJSONFromFile.stylesheet_path;
    }

    if(configJSONFromFile.highlight_stylesheet_path != null){
        defaultConfigJSON.highlight_stylesheet_path = configJSONFromFile.highlight_stylesheet_path;
    }

    if(configJSONFromFile.use_internal_stylesheet != null){
        defaultConfigJSON.use_internal_stylesheet = configJSONFromFile.use_internal_stylesheet;
    }

    if(configJSONFromFile.insert_page_breaks != null){
        defaultConfigJSON.insert_page_breaks = configJSONFromFile.insert_page_breaks;
    }

    if(configJSONFromFile.project_files_list != null){
        defaultConfigJSON.project_files_list = configJSONFromFile.project_files_list;
    }

    if(configJSONFromFile.show_errors_in_output_html != null){
        defaultConfigJSON.show_errors_in_output_html = configJSONFromFile.show_errors_in_output_html;
    }

    
    if(configJSONFromFile.auto_include_header_footer_nav != null){
        defaultConfigJSON.auto_include_header_footer_nav = configJSONFromFile.auto_include_header_footer_nav;
    }

    if(configJSONFromFile.auto_generate_html_on_file_change != null){
        defaultConfigJSON.auto_generate_html_on_file_change = configJSONFromFile.auto_generate_html_on_file_change;
    }

    if(configJSONFromFile.auto_generate_html_on_include_file_change != null){
        defaultConfigJSON.auto_generate_html_on_include_file_change = configJSONFromFile.auto_generate_html_on_include_file_change;
    }

    if(configJSONFromFile.markdown_it_toc_settings != null){
        defaultConfigJSON.markdown_it_toc_settings = configJSONFromFile.markdown_it_toc_settings;
    }
  
    if(configJSONFromFile.markdown_it_anchor_settings != null){
        defaultConfigJSON.markdown_it_anchor_settings = configJSONFromFile.markdown_it_anchor_settings;
    }

    if(configJSONFromFile.markdown_it_checkbox_settings != null){
        defaultConfigJSON.markdown_it_checkbox_settings = configJSONFromFile.markdown_it_checkbox_settings;
    }

    if(configJSONFromFile.markdown_it_emoji_settings != null){
        defaultConfigJSON.markdown_it_emoji_settings = configJSONFromFile.markdown_it_emoji_settings;
    }

    if(configJSONFromFile.pdfChromeExecutablePath != null){
        defaultConfigJSON.pdfChromeExecutablePath = configJSONFromFile.pdfChromeExecutablePath;
    }

    if(configJSONFromFile.pdfProjectFilesList != null){
        defaultConfigJSON.pdfProjectFilesList = configJSONFromFile.pdfProjectFilesList;
    }

    if(configJSONFromFile.pdfScale != null){
        defaultConfigJSON.pdfScale = configJSONFromFile.pdfScale;
    }

    if(configJSONFromFile.pdfPaperFormat != null){
        defaultConfigJSON.pdfPaperFormat = configJSONFromFile.pdfPaperFormat;
    }

    if(configJSONFromFile.pdfDisplayHeaderFooter != null){
        defaultConfigJSON.pdfDisplayHeaderFooter = configJSONFromFile.pdfDisplayHeaderFooter;
    }

    if(configJSONFromFile.pdfHeaderTemplate != null){
        defaultConfigJSON.pdfHeaderTemplate = configJSONFromFile.pdfHeaderTemplate;
    }

    if(configJSONFromFile.pdfFooterTemplate != null){
        defaultConfigJSON.pdfFooterTemplate = configJSONFromFile.pdfFooterTemplate;
    }

    if(configJSONFromFile.pdfPrintBackgroundGraphics != null){
        defaultConfigJSON.pdfPrintBackgroundGraphics = configJSONFromFile.pdfPrintBackgroundGraphics;
    }

    if(configJSONFromFile.pdfOrientationLandscape != null){
        defaultConfigJSON.pdfOrientationLandscape = configJSONFromFile.pdfOrientationLandscape;
    }

    if(configJSONFromFile.pdfPageRanges != null){
        defaultConfigJSON.pdfPageRanges = configJSONFromFile.pdfPageRanges;
    }

    if(configJSONFromFile.pdfWidth != null){
        defaultConfigJSON.pdfWidth = configJSONFromFile.pdfWidth;
    }

    if(configJSONFromFile.pdfHeight != null){
        defaultConfigJSON.pdfHeight = configJSONFromFile.pdfHeight;
    }

    if(configJSONFromFile.pdfMarginTop != null){
        defaultConfigJSON.pdfMarginTop = configJSONFromFile.pdfMarginTop;
    }

    if(configJSONFromFile.pdfMarginRight != null){
        defaultConfigJSON.pdfMarginRight = configJSONFromFile.pdfMarginRight;
    }

    if(configJSONFromFile.pdfMarginBottom != null){
        defaultConfigJSON.pdfMarginBottom = configJSONFromFile.pdfMarginBottom;
    }

    if(configJSONFromFile.pdfMarginLeft != null){
        defaultConfigJSON.pdfMarginLeft = configJSONFromFile.pdfMarginLeft;
    }

    if(configJSONFromFile.pdfPreferCSSPageSize != null){
        defaultConfigJSON.pdfPreferCSSPageSize = configJSONFromFile.pdfPreferCSSPageSize;
    }

    return defaultConfigJSON;
}

module.exports = {
    getConfigJSON: function(fileUri) {

        if(! fileUri){
            return '';
        }

        var configFilePath = '';
        var rootFolder = this.getRootFolder(fileUri);
        if(rootFolder){
            configFilePath = path.join(rootFolder, path.sep, configFileName);
        }

        if (configFilePath){

            // Get project configuration JSON 
            try{
                fs.accessSync(configFilePath, fs.constants.F_OK | fs.constants.R_OK);

                var projectConfigRaw = fs.readFileSync(configFilePath);
                
                var configurationJSONObject = defaultConfigurationJSONObject;

                if (projectConfigRaw && projectConfigRaw.length > 0){
                    
                    // Get JSON object from file
                    var fileConfigurationJSONObject = JSON.parse(projectConfigRaw);
                    
                    // Transfer values read from file to internal Configuration JSON Object
                    if (fileConfigurationJSONObject){
                        configurationJSONObject = updateConfigurationValuesFromFile(configurationJSONObject, fileConfigurationJSONObject);
                    }
                    else
                    {
                        console.error('::config_worker:: Error parsing project configuration file. Default configuration values will be used.');
                    }
                    
                    // Return internal Configuration JSON Object with values read from file
                    return configurationJSONObject;
                }
            }
            catch(e){
                console.error('::config_worker:: Error accessing project configuration file. ' + e.message);
                vscode.window.showErrorMessage('Error accessing project configuration file.. \r\n' + e.message);        
                return '';
            }   
        }
        else{
            console.error('::config_worker:: MarkdownDox project configuration file not found.');
            return '';
        }
    },

    getRootFolder: function(fileUri){
        
        if(! fileUri){
            return '';
        }
        
        var rootFolder = '';
        
        try{
            var currentFolderPath = path.dirname(fileUri.fsPath); 
            var currentFolderPathArray = currentFolderPath.split(path.sep);
            var JSONConfigPath = path.join(currentFolderPathArray.join(path.sep), configFileName);

            //Find configuration file - this file MUST be in the project root!
            while (! fs.existsSync(JSONConfigPath)){
                
                if(currentFolderPathArray.length > 0){
                    currentFolderPathArray.pop();
                    JSONConfigPath = path.join(currentFolderPathArray.join(path.sep), configFileName);   
                }
                else{
                    JSONConfigPath = '';
                    break;
                }
            };

            rootFolder = path.dirname(JSONConfigPath);
        }
        catch(e){
            console.error('::config_worker:: Error getting root folder: ', e.message);
        }

        return rootFolder;
    },

    getDefaultConfigurationJSON: function() {
        return JSON.stringify(defaultConfigurationJSONObject, null, " \t");
    },

    getDefaultConfigurationObject: function() {
        return defaultConfigurationJSONObject;
    }
};
