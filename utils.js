var fs = require('fs-extra'); //Adds support for copying folders
var fsold = require('fs');
var path = require('path');

module.exports = {

    copyFolderAndContentToOutput: function (fromFolder, toFolder){

        try{

            //Copy folder and all contained files to output folder
            fs.emptyDirSync(toFolder); //Delete folder contents

            var subFolders = [];

            if(fs.existsSync(fromFolder)) {
                var dirContents = fs.readdirSync(fromFolder);
                for (var i = 0; i < dirContents.length; i++){
                    if(fs.statSync(path.join(fromFolder, dirContents[i])).isDirectory()){
                        subFolders.push(dirContents[i]);
                    }
                }

                fs.copySync(fromFolder, toFolder, {overwrite: true});

                if(subFolders && subFolders.length > 0){
                    for(var i = 0; i < subFolders.length; i++){
                        fs.emptyDirSync(path.join(toFolder, subFolders[i]));

                        if (fs.existsSync(path.join(toFolder, subFolders[i]))){
                                           
                            fs.copySync(path.join(fromFolder, subFolders[i]), path.join(toFolder, subFolders[i]), {overwrite: true});

                        }
                    }
                    
                }
            }
        }
        catch(e){
            console.error('::utils:: Error copying folder and all contained files to output folder: ' + e.message);
            return;
        }
    }
}