# MarkdownDox TODO List

## Must have
- [x] Modify page cross reference links to point to anchor in single page when creating single page from all project files
- [x] Do not copy docs folder when creating new project - filewatcher is triggered many times on project create.  

## Nice to have
- [ ] Improve loading of markdown-it plugins. 
    - [ ] Use Array map.
- [ ] Create more unit tests
- [ ] Integrate into CI
- [ ] Add automatic 'npm install' post install



