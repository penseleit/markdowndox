const vscode = require('vscode');
var path = require('path');
var html_generator = require('./html_generator');
var pdf_generator = require('./pdf_generator');
var md_generator = require('./markdown_generator');
var mdox_project = require('./mdox_project');
var config_worker= require('./config_worker');
var watcher = null;
var config_filename = 'mdox_config.json';

function getFileUri(fileUri, extension1, extension2){
    if (! fileUri){
            
        fileUri = vscode.window.activeTextEditor.document.uri;
        
        if ((path.extname(fileUri.fsPath) != extension1 && path.extname(fileUri.fsPath) != extension2) || fileUri.scheme != "file"){
            return '';
        }
    }

    return fileUri;
}

function activate(context) {

    console.log('Extension "MarkdownDox" is now active.');

    // Create new MarkdownDox project
    let dispProject = vscode.commands.registerCommand('extension.createMDoxProject', function () {
        mdox_project.createMDoxProject();
    });

    // Generate single HTML from from current Markdown file
    let dispHTMLFromCurrent = vscode.commands.registerCommand('extension.makeMDoxHTMLFromCurrentMDFile', function (fileUri) {

       fileUri = getFileUri(fileUri, '.md', '.markdown');

        html_generator.createFileHTML(fileUri);
    });

    // Generate HTML from all project Markdown files
    let dispHTMLFromAll = vscode.commands.registerCommand('extension.makeMDoxHTMLFromAllProjectMDFiles', function (fileUri) {

        fileUri = getFileUri(fileUri, '.md', '.markdown');
                
        html_generator.createProjectHTML(fileUri);
    });
    
    // Make a single Markdown file from all project files
    let dispMDFromAll = vscode.commands.registerCommand('extension.makeMDoxMDSingleFromAllProjectMDFiles', function (fileUri) {

        fileUri = getFileUri(fileUri, '.md', '.markdown');
        
        md_generator.createSingleMarkdownFile(fileUri);
    });
    
    // Make a PDF file from the selected HTML file
    let dispPDFFromCurrent = vscode.commands.registerCommand('extension.makeMDoxPDFFromCurrentHTMLFile', function (fileUri) {
                       
        fileUri = getFileUri(fileUri, '.html', '.htm');

        pdf_generator.createFilePDF(fileUri);
    });

     // Make a PDF file from all project files in one pass
     let dispPDFFromAll = vscode.commands.registerCommand('extension.makeMDoxPDFFromAllProjectFiles', function (fileUri) {
                       
        fileUri = getFileUri(fileUri, '.md', '.markdown');

        var singleMDUri = md_generator.createSingleMarkdownFile(fileUri);

        if (singleMDUri){
            var singleHTMLUri = html_generator.createFileHTML(singleMDUri);

            if (singleHTMLUri){
                pdf_generator.createFilePDF(singleHTMLUri);
            }
            else{
                console.error('::extension:: Error processing PDF file. Unable to create PDF file from ' + singleHTMLUri.fsPath);
                vscode.window.showErrorMessage('Error processing PDF file. Unable to create PDF file from ' + singleHTMLUri.fsPath);
            }
        }
        else{
            console.error('::extension:: Error processing Markdown file. Unable to create Markdown file ' + singleMDUri.fsPath + ' from project files.');
            vscode.window.showErrorMessage('Error processing Markdown file. Unable to create Markdown file ' + singleMDUri.fsPath + ' from project files.');
        }
    });
    
    //Add subscriptions so they dispose correctly on extension deactivation
    context.subscriptions.push(dispProject);
    context.subscriptions.push(dispHTMLFromCurrent);
    context.subscriptions.push(dispHTMLFromAll);
    context.subscriptions.push(dispMDFromAll);
    context.subscriptions.push(dispPDFFromCurrent);
    context.subscriptions.push(dispPDFFromAll);
    context.subscriptions.push(watcher);

    //Setup file watcher
    watcher = vscode.workspace.createFileSystemWatcher('**/*.{md, markdown}', true, false, true); 

    watcher.onDidChange(function (eventURI) { 
        html_generator.fileChangeEventHandler(eventURI);  
    });
   


   //Extend Markdown-It features in the built-in Preview 
   return {
        extendMarkdownIt(md) {
            return md.use(require('markdown-it-abbr'))
                .use(require('markdown-it-anchor'))    
                .use(require('markdown-it-checkbox'))
                .use(require('markdown-it-container'), 'note')
                .use(require('markdown-it-container'), 'warning')
                .use(require('markdown-it-container'), 'caution')
                .use(require('markdown-it-container'), 'danger')
                .use(require('markdown-it-container'), 'important')
                .use(require('markdown-it-container'), 'tip')
                .use(require('markdown-it-container'), 'info')
                .use(require('markdown-it-decorate'))
                .use(require('markdown-it-deflist'))
                .use(require('markdown-it-emoji'))
                .use(require('markdown-it-footnote'))
                .use(require('markdown-it-ins'))
                .use(require('markdown-it-kbd'))
                .use(require('markdown-it-mark'))
                .use(require('markdown-it-sup'))
                .use(require('markdown-it-sub'))
                .use(require('markdown-it-table-of-contents'))
                .use(require('markdown-it-underline'))
                .use(require('markdown-it-multimd-table'), {enableMultilineRows: true});
                
        }
    }
}
exports.activate = activate;


function deactivate() {
}

exports.deactivate = deactivate;
