[![](https://vsmarketplacebadge.apphb.com/version-short/penseleit.markdowndox.svg)](https://marketplace.visualstudio.com/items?itemName=penseleit.markdowndox)
[![](https://vsmarketplacebadge.apphb.com/downloads-short/penseleit.markdowndox.svg)](https://marketplace.visualstudio.com/items?itemName=penseleit.markdowndox)
[![](https://vsmarketplacebadge.apphb.com/rating-short/penseleit.markdowndox.svg)](https://marketplace.visualstudio.com/items?itemName=penseleit.markdowndox)

# Changes to take note of!

In the latest release **the way that the current page is highlighted in the sidebar Table of Contents has been improved**.

Instead of highlighting the entire `<li>` list element, now just the `<a>` anchor element is highlighted. 
This means that it is easier to style multiple levels of list items.

When you install the latest version of MarkdownDox, regenerate all of your Markdown files as HTML so that they all pick up this improvement.

If you are not using the upgraded default stylesheets, add this to your custom stylesheets:

```
.selected_nav_item {
  color: <your color> !important; 
  display: block;
}
```

Use of the !important identifier applies the desired colour to the `<a>` element.


# MarkdownDox

MarkdownDox is a simple documentation tool for Visual Studio Code. It is designed to help Technical Writers (or any writers for that matter) to utilise Visual Studio Code's Markdown editing capabilities to easily create documentation which outputs to static HTML and PDF files.

Don't expect all the bells and whistles; this extension just does the basics. 

![](https://gitlab.com/penseleit/markdowndox/raw/master/images/CreateNewProject.gif)

## Features

- Powered by Markdown-It markdown parser with additional plugins to provide an extended Markdown syntax
- Per project configuration file
- Support for Markdown `include` files to include common content within Markdown files, such as header, footer, etc.
- Provided with a set of [*flexbox*](https://www.w3schools.com/css/css3_flexbox.asp) based stylesheets designed to be responsive for the multiple screen sizes, including laptops and mobile phones
- Outputs [HTML5 semantic HTML layout](https://www.w3schools.com/html/html5_semantic_elements.asp), including `<header>`, `<nav>`, `<section>`, and `<footer>` elements
- Generates PDF files using Google Chrome's [*puppeteer*](https://github.com/GoogleChrome/puppeteer) framework
- Options for internal or external stylesheets
- Output to a configurable output folder
- Create and use your own custom stylesheets if desired
- Provides a single point of navigation in a configurable, project-level  Table of Contents (read from an `include` file)
- The option of including a page-level Table of Contents
- Written in Javascript, using Node.js as its processing engine
- Designed to be simple and easy to use for less technical users
- Output is designed to be readable in HTML or PDF without a connection to the internet, for instance in locked-down computing environments where internet access is restricted

### Functionality included

The following functions are available to the user:

1. Create a *new* MarkdownDox project - `Ctrl+Alt+n`
2. Combine all project Markdown into a *single* file - `Ctrl+Alt+s`
3. Generate HTML from *this* file - `Ctrl+Alt+t`
4. Make *PDF* from this file - `Ctrl+Alt+p`
5. Generate HTML from *all* project files - `Ctrl+Alt+a`
6. Make PDF from all project files (*export* all to a single PDF) - `Ctrl+Alt+e`
7. Keyboard shortcuts as shown for the functionality listed above


### Markdown-It plugins included

Markdown-It plugins included in MarkdownDox:

- `markdown-it-abbr`
- `markdown-it-anchor`
- `markdown-it-checkbox`
- `markdown-it-container` supporting `info`, `important`, `danger`, `caution`, `warning` and `note` admonitions
- `markdown-it-decorate`
- `markdown-it-deflist`
- `markdown-it-emoji`
- `markdown-it-footnote`
- `markdown-it-ins`
- `markdown-it-kbd`
- `markdown-it-mark`
- `markdown-it-multimd-table`
- `markdown-it-sup`
- `markdown-it-sub`
- `markdown-it-table-of-contents`
- `markdown-it-underline`

#### Sample 

A sample of the extended Markdown that these plugins enable can be seen by clicking the link below. 

[Sample](https://penseleit.gitlab.io/markdowndox/Sample.html)

## Requirements

- Visual Studio Code version: 1.30.2 and above.
- A web browser which supports `HTML5` with `semantic layout` elements and `flexbox` CSS styling
    - Minimum of Chrome 29.0, Edge 11.0, Firefox 22.0, Internet Explorer 10, Opera 48.
- Node.js version 8.9.3 and above.
- Access to the internet to download `npm` packages.

## Recommended extensions

MarkdownDox can be complemented with the extensions listed below to make the task of documentation simpler for you.

- [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) - launch a local web server and display your output HTML in a web browser with live updates upon file save events.
- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) - spell check your Markdown and your writing.
- [Markdown Table Prettifier](https://marketplace.visualstudio.com/items?itemName=darkriszty.markdown-table-prettify) - cleans up and aligns Markdown table syntax to make it more easily readable.
- [Markdown Helper](https://marketplace.visualstudio.com/items?itemName=joshbax.mdhelper) - provides Markdown formatting keyboard shortcuts and context menu items.

## Installation

### Install from Visual Studio Code

Install MarkdownDox from Visual Studio Code:

1. Launch Visual Studio Code and Open the Extensions View from the Activity Bar on the left of the window.
2. Search for the MarkdownDox Extension
3. Click the *Install* button and let the installation finish
4. Click the *Reload* button to allow VS Code to load the Extension.
5. In the command prompt window, navigate to the root of the MarkdownDox extension folder:


    - Windows `%USERPROFILE%\.vscode\extensions\markdowndox`
    - macOS `~/.vscode/extensions/markdowndox`
    - Linux `~/.vscode/extensions/markdowndox`

6. Type `npm install` to install the dependency files into the `node_modules` folder in 
the extension folder.

**Note:** You will have to run `npm install` again each time an updated version of MarkdownDox is installed (even if it auto-installs in Visual Studio Code) in the new extension folder which will have the version number added as a suffix to the extension name, e.g. `penseleit.markdowndox-1.1.0`.

### Install from a VSIX file

Install the MarkdownDox Visual Studio Code extension from a VSIX file on the command line:

1. Download the VSIX file from the Visual Studio Code Extension Marketplace
2. Open a command prompt and type:

        code --install-extension markdowndox-1.0.0.vsix  

3. In the command prompt window, navigate to the root of the MarkdownDox extension folder:


    - Windows `%USERPROFILE%\.vscode\extensions\markdowndox`
    - macOS `~/.vscode/extensions/markdowndox`
    - Linux `~/.vscode/extensions/markdowndox`

4. Type `npm install` to install the dependency files into the `node_modules` folder in 
the extension folder.

## Extension configuration

MarkdownDox does not have any extension-wide configuration options. All configuration is done at the project level.

## Project configuration

Each MarkdownDox documentation project has its own project configuration file titled `mdox_config.json`. 

The project configuration file uses the JSON file format. Configuration values can be independently set for each project.

### Project configuration options

Below are the available project configuration options with default values.

**project_name: "My MarkdownDox Project"**

The name of the documentation project.

**images_folder: "./images"**

The folder where project images are sourced from, relative to the project root folder. This folder and all of its contents will be copied to the output folder. Only one level of sub folder is supported within this folder, e.g. "./images/aircraft" will be copied to the output folder but "./images/aircraft/helicopters" will not.

**stylesheet_folder: "./css"**

The folder where project stylesheets are sourced from, relative to the project root folder. This folder and all of its contents will be copied to the output folder. No sub folders within this folder will be copied.

**includes_folder: "./includes"**

The folder where project includes are sourced from, relative to the project root folder. This folder and all of its contents will be copied to the output folder. No sub folders within this folder will be copied. Include files are not processed recursively. In other words, include statements within include files will not be processed.

To include an include file in you Markdown, add the `include` statement on a line of its own with an empty line before and after it, e.g.:

    include::(includes/header.md)

**project_output_folder: "./docs"**

The folder where project output files are written to. Any existing files are overwritten. The images and stylesheet folders are deleted and re-copied from the project image and stylesheet folders each time a new HTML output file or set of HTML output files is created.

**stylesheet_path: "./css/mdox_default.css"**

The path to the project stylesheet, relative to the project root folder. This stylesheet is used to style each output HTML and PDF file created.

**highlight_stylesheet_path: "./css/default.css"**

The path to the project highlight stylesheet, relative to the project root folder. The highlight stylesheet is used for syntax highlighting of code content in each output HTML and PDF file created.

**use_internal_stylesheet: false**

If true, then the contents of the stylesheet configured in the stylesheet_path option will be copied into the \<head>..\</head> section of the output HTML file and placed within the \<style>...\</style> tags. 

If false, a \<link> tag will be added within the  \<head>..\</head> section of the output HTML file, linking to the stylesheet configured in the stylesheet_path option. For example:

    <link rel="stylesheet" type="text/css" href="css/mdox_default.css" />

**project_files_list:[ "Help.md", "Sample.md" ]**

The list of Markdown files which are included as part of the project. The files listed here are each generated as separate HTML files when the user selects to *Generate HTML from all project files*. 

If a file is not listed here, it will not be generated as HTML during this process. 

**show_errors_in_output_html: true**

If true, when an include file is not found during processing, a message is inserted into the output HTML file - `INCLUDE FILE CONTENTS NOT FOUND`.

If false, the output HTML file is generated without the missing include content. 

**auto_include_header_footer_nav: true**

If true, the project include files `header.md`, `footer.md` and `navigation.md` are automatically processed and included in the output HTML files, even if they are not explicity included using the `include` statement in the source Markdown files. 

If this option is set to true and you do not want to include content for the header, footer or navigation, then leave the appropriate file contents blank. For example, if you do not want to have navigation (Table of Contents), then leave the `navigation.md` file blank.

If false, then only explicitly declared `include` statements will be processed and included in the output HTML file. 

To include header, footer and navigation when this option is set to false, add the `include` statement on a line of its own with an empty line before and after it, e.g.:

    include::(includes/header.md)

**insert_page_breaks: true**

If true, a page break tag `<div class="page" />` is added between each Markdown file processed when the user selects to *Combine all project Markdown into a single file* or *Make PDF from all project files*.

When the resultant single Markdown file is made into a PDF file (by selecting *Make PDF from this file* or as part of the *Make PDF from all project files* process), each page break tag will result in a page break in the output PDF file. 

**auto_generate_html_on_file_change: true**

When true, whenever a file change is detected (saved) in a Markdown file (`.md`/`.markdown`), MarkdownDox will automatically generate a new HTML file from the changed Markdown file.

**auto_generate_html_on_include_file_change: true**

When true, whenever a file change is detected (saved) in an include Markdown file (`.md`/`.markdown`), MarkdownDox will automatically generate new HTML files for all project Markdown files.

**markdown_it_toc_settings:** 

```
{
    "includeLevel": [2, 3, 4],
    "containerClass": "table-of-contents",
    "slugify": "(s) => encodeURIComponent(String(s).trim().toLowerCase().replace(/\s+/g, '-'))",
    "markerPattern": "/^\\[\\[toc\\]\\]/im",
    "listType": "ul",
    "format": "",
    "forceFullToc": false,
    "containerHeaderHtml": "",
    "containerFooterHtml": ""
}
```

Settings for the Markdown-It Table of Contents plugin. See the [plugin website](https://github.com/Oktavilla/markdown-it-table-of-contents) for details.

**markdown_it_anchor_settings:** 

``` 
{
    "level": 1,
    "slugify": "",
    "permalink": false,
    "renderPermalink": "",
    "permalinkClass": "header-anchor",
    "permalinkSymbol": "¶",
    "permalinkBefore": false,
    "permalinkHref": "",
    "callback": ""
}
```

Settings for the Markdown-It Anchor plugin. See the [plugin website](https://github.com/valeriangalliat/markdown-it-anchor) for details.


**markdown_it_checkbox_settings:** 

```
{
    "divWrap": true,
    "divClass": "cb",
    "idPrefix": "cbx_"
}
```

Settings for the Markdown-It Checkbox plugin. See the [plugin website](https://github.com/mcecot/markdown-it-checkbox) for details.

**markdown_it_emoji_settings:** 

```
{
    "defs": "",
    "enabled": "",
    "shortcuts": ""

}
```

**pdfProjectFilesList: [ "Help.md", "Sample.md" ]**

The list of Markdown files which are included as part of the project when generating a single Markdown file for use in making a PDF output file. 

The files listed here are each processed and joined together, in the order they are listed, into a single Markdown file. This happens when the user selects to *Combine all project Markdown into a single file* or to *Make PDF from all project files*. 

This setting allows the PDF output to include more or less content than the HTML output. For example, the PDF file may include a document control section which is not needed in the HTML output.

If a file is not listed here, it will not be included in the single Markdown file or the output PDF during this process. 

#### PDF options

The options below are passed to the [puppeteer](https://github.com/GoogleChrome/puppeteer) library which creates the output PDF file. For information on these settings, please refer to the [puppeteer *page.pdf([options])* page](https://github.com/GoogleChrome/puppeteer/blob/v1.11.0/docs/api.md#pagepdfoptions). 

**pdfChromeExecutablePath: ""**

**pdfScale: 1**

**pdfPaperFormat: "A4"**

**pdfDisplayHeaderFooter: true**

**pdfHeaderTemplate: " "** *Note: It is important to keep a space between the quotes if you do not want to display any header content in the PDF output.*

**pdfFooterTemplate: "\<div style=\"width: 100%; font-family: Arial; font-size: 9px; margin 0 auto; text-align: center \">\<span style=\"margin-left: 20px;\">Copyright &copy; 2019 My Company</span> Page \<span class='pageNumber'>\</span> of \<span class='totalPages'>\</span>\</div>"**

**pdfPrintBackgroundGraphics: true**

**pdfOrientationLandscape: false**

**pdfPageRanges: ""**

**pdfWidth: ""**

**pdfHeight: ""**

**pdfMarginTop: "1cm"**

**pdfMarginRight: ""**

**pdfMarginBottom: "1cm"**

**pdfMarginLeft: ""**

**pdfPreferCSSPageSize: false**


## Usage

### Getting started

Read about how to use MarkdownDox on the [Getting started](https://penseleit.gitlab.io/markdowndox/GettingStarted.html) page.

### Supported Markdown Syntax 

See a sample of supported Markdown syntax on the [Sample](https://penseleit.gitlab.io/markdowndox/Sample.html) page.

### Creating custom stylesheets

Customise the look of your HTML and PDF output by [creating custom stylesheets](https://penseleit.gitlab.io/markdowndox/CreatingCustomStylesheets.html). 

## Known Issues


As the aim of this extension is to provide a simple solution for creating static HTML and PDF output, some limitations exist in its functionality:

- It is assumed that all project Markdown files will be kept in the project root folder and not in any sub folders under it.
- It is assumed that the `includes` folder, as defined in the project configuration file, is located directly in the project root folder and not in any sub folders under it.
- Only one level of indentation is supported for the list items in the `project_toc` Table of Contents located on the left side of the HTML output by default. More levels may be added by the user if desired by modifying the stylesheets.
- MarkdownDox does not cater for customising most of the included `markdown-it` plugins in this build, apart form `markdown-it-table-of-contents`.

## Release Notes


See [CHANGELOG](https://gitlab.com/penseleit/markdowndox/raw/master/CHANGELOG.md).

-------------------------------------------------------------------

