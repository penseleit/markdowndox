# Change Log

All notable changes to the MarkdownDox extension will be documented in this file.

### 1.0.0

- Initial release of MarkdownDox.

### [1.0.1]
- Tidying up a few things

### [1.0.2]
- Tidying up a few more things

### 1.0.3
- Improved function which converts external links to internal anchors when creating HTML from a single file.
- Fixed CSS so Flexbox works properly with IE 11 and Edge.

### 1.1.0
- Bump up a minor SemVer

### 1.1.1
- Added support for better table formatting with `markdown-it-multimd-table` plugin.
- Added file watcher to Workspace root. Any single Markdown file that changes is auto-generated as HTML. Any Markdown file in includes folder triggers HTML generation of all project files. Works well with Ritwick Dey's Live Server extension.
- Added configuration settings (`auto_generate_html_on_file_change` and `auto_generate_html_on_include_file_change`) to enable/disable automatic generation of HTML from Markdown when a file change (save) is detected.
- Added configuration options for `markdown-it-table-of-contents` plugin. See [markdown-it-table-of-contents npm package](https://www.npmjs.com/package/markdown-it-table-of-contents) for details.
- Add configuration options for `markdown-it-emoji`, `markdown-it-anchor` and `markdown-it-checkbox`.

### 2.0
- Improved the highlighting of the current page in the sidebar Table of Contents.
_ Added styles to enable two levels of list item in sidebar Table of Contents.
- Added some unit tests
- Added bahaviour to convert in-project links (between separate pages) to internal links when all project files are combined into a single Markdown file/HTML file.