
include::(includes/header.md)

include::(includes/navigation.md)

# Getting started

To quickly get started writing documentation for your new project with MarkdownDox follow the steps below:

## Configure

1. Configure your project's details in the file `mdox_config.json`.
    - Set one of the built-in stylesheets to change the look of your output: 
        - mdox_default.css (default)
        - mdox_official.css 
        - mdox_simple.css 
        - mdox_simple-sans.css
        - mdox_writer.css.

2. Edit and save the include files `header.md` and `footer.md` as desired.
    - These files provide content for the header and footer of each generated HTML file if you add them through include statements in your Markdown file, or if you enable the configuration option `auto_include_header_footer_nav`.
    - You can use raw HTML in your markdown files and it will be passed unchanged to the output HTML file.

## Write

1. Create new Markdown files with the file extension `.md`.
2. Write Markdown content and save the changes. 
    - See the [sample page](Sample.html) for supported Markdown syntax.
    - Add *include* statements to include content from external files (if required). For example:

        `include::(includes/content_to_include.md)`

3. Add the name of each new file to the `project_files_list` and `pdfProjectFilesList` sections of the configuration file. For example:

    ```
    "project_files_list": [
        Introduction.md,
        Create a new spanner.md,
        Tighten a nut.md
    ]
    ```

4. Add a link to each new file in an unsorted list, but with the extension `.html` instead of `.md` to the file `navigation.md` in the `includes` folder. For example, for the Markdown file `Introduction.md`, you create the *Introduction* link below:
    
    ```
    ## Contents

    - [Introduction](Introduction.html)
    - [Create a new spanner](Create a new spanner.html)
    - [Tighten a nut](Tighten a nut.html)
    ```

## Publish using keyboard shortcuts

1. Open a Markdown file and type `Ctrl+Alt+t` to generate HTML from the file that is currently open in the editor.
2. Open a Markdown file and type `Ctrl+Alt+a` to generate HTML for each file listed in the `project_files_list` configuration option.
3. Open a Markdown file and type `Ctrl+Alt+s` to generate a single Markdown file from all of the files listed in the `pdfProjectFilesList` configuration option joined together in the list's order.
4. Open a HTML file and type `Ctrl+Alt+p` to generate a PDF file from the file that is currently open in the editor.
5. Open a Markdown file and type `Ctrl+Alt+e` to generate a PDF file from all of the files listed in the `pdfProjectFilesList` configuration option joined together in the list's order

## Publish using context menus

You can also access the same functionality using the context menu in the *File Explorer* pane.

Right-click with your mouse anywhere in the File Explorer pane:

- MDox: Create a *new* MarkdownDox project

Right-click with your mouse on a Markdown file in the File Explorer pane:

- MDox Generate HTML from *this* file
- MDox: Combine all project Markdown into a *single* file
- MDox: Generate HTML from *all* project files
- MDox: Make PDF from all project files

Right-click with your mouse on a HTML file in the File Explorer pane:

- MDox: Make *PDF* from this file

## Use your output

1. The output is written to the output folder as defined in the `project_output_folder` configuration option (`docs` by default).
2. Copy this output to provide documentation for your product, or include the whole project and its output in your source code repository.


And there you go! You have created HTML and PDF documentation from Markdown source text.


# Advanced usage

## Use raw HTML in Markdown

When the extended Markdown supported by MarkdownDox does not give you fine enough control of your output, you can insert raw HTML snippets where needed directly into the Markdown source.

The raw HTML is passed through the HTML generation process unchanged and will be included in the HTML output of your project.

## Add custom page breaks for finer control of your PDF output

To fine-tune the output of your PDF files and create page breaks exactly where you need them:

1. Create a *single* Markdown file from all of the files in your project - `Ctrl+Alt+s`.
2. Edit the *single* Markdown file and enter this HTML snippet wherever a page break is desired:

    `<div class="page" />`

3. Generate a PDF from this *single* Markdown file.
4. Review and repeat the steps above until your PDF file has all of the desired page breaks in the right places.

## Create custom stylesheets to change the look and feel of the output

You can create your own custom stylesheets and use them to style your output HTML and PDF files.

1. Create your custom stylesheet following the guidelines in the [Creating custom stylesheets](CreatingCustomStylesheets.html) page.
2. Copy the new stylesheet files to the default MarkdwonDox extension `project_template\css` folder or create a folder of your own and copy the files there.
3. Add the path to your desired custom stylesheet file in `mdox_config.json` for the configuration option `stylesheet_path`.
4. Generate HTML or PDF files using your custom template.

include::(includes/footer.md)
