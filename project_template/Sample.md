# Sample Markdown 

## Showing Markdown supported by MarkdownDox

Thanks to [Markdown-It](https://markdown-it.github.io/) for providing the original sample content.

### [Table of Contents](https://www.npmjs.com/package/markdown-it-table-of-contents)

`[[toc]]` produces the Table of Contents below.

[[toc]]

# Headings

`# h1 Heading`

# h1 Heading

`## h2 Heading`

## h2 Heading

`### h3 Heading`

### h3 Heading

`#### h4 Heading`

#### h4 Heading

`##### h5 Heading`

##### h5 Heading

`###### h6 Heading`

###### h6 Heading


## Horizontal Rules

`___`

___

`---`

---

`***`

***


## Typographic replacements

Enable typographer option to see result.

`(c) (C) (r) (R) (tm) (TM) (p) (P) +-`

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

`test.. test... test..... test?..... test!....`

test.. test... test..... test?..... test!....

`!!!!!! ???? ,,  -- ---`

!!!!!! ???? ,,  -- ---

`"Smartypants, double quotes" and 'single quotes'`

"Smartypants, double quotes" and 'single quotes'

## Emphasis

`**This is bold text**`

**This is bold text**

`__This is bold text__`

__This is bold text__

`*This is italic text*`

*This is italic text*

`~~Strikethrough~~`

~~Strikethrough~~


## Blockquotes


```
> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.
```

> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

Unordered

```
+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!
```

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

```
1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa
```

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa

```
1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`
```

1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`


Start numbering with offset:

```
57. foo
1. bar
```

57. foo
1. bar


## Code

```
Inline  `code`
```

Inline `code`

Indented code

```
    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code
```

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


Block code "fences"


\`\`\` 

`Sample text here...`

\`\`\`

```

Sample text here...

```

Syntax highlighting

\`\`\` `js`

`var foo = function (bar) {`

  `return bar++;`

`};`

`console.log(foo(5));`

\`\`\`

``` js

var foo = function (bar) {

return bar++;

};

console.log(foo(5));

```

## Tables

```
| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |
```

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


Right aligned columns

```
| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |
```

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


## Links

`[link text](http://dev.nodeca.com)`

[link text](http://dev.nodeca.com)

`[link with title](http://nodeca.github.io/pica/demo/ "title text!")`

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

`Autoconverted link https://github.com/nodeca/pica (enable linkify to see)`

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)


## Images


`![Minion](https://octodex.github.com/images/minion.png)`

![Minion](https://octodex.github.com/images/minion.png)

`![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")`

![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

`![Alt text][id]`

![Alt text][id]

With a reference later in the document defining the URL location:

`[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"`

[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"


## Plugins

The killer feature of `markdown-it` is very effective support of
[syntax plugins](https://www.npmjs.org/browse/keyword/markdown-it-plugin).


### [Emojies](https://github.com/markdown-it/markdown-it-emoji)

```
> Classic markup: :wink: :crush: :cry: :tear: :laughing: :yum:
>
> Shortcuts (emoticons): :-) :-( 8-) ;)
```

> Classic markup: :wink: :crush: :cry: :tear: :laughing: :yum:
>
> Shortcuts (emoticons): :-) :-( 8-) ;)

see [how to change output](https://github.com/markdown-it/markdown-it-emoji#change-output) with twemoji.


### [Subscript](https://github.com/markdown-it/markdown-it-sub) / [Superscript](https://github.com/markdown-it/markdown-it-sup)

```
- 19^th^
- H~2~O
```

- 19^th^
- H~2~O


### [\<ins>](https://github.com/markdown-it/markdown-it-ins)


`++Inserted text++`

++Inserted text++


### [\<mark>](https://github.com/markdown-it/markdown-it-mark)

`==Marked text==`

==Marked text==


### [Footnotes](https://github.com/markdown-it/markdown-it-footnote)

```
Footnote 1 link[^first].

Footnote 2 link[^second].

Inline footnote^[Text of inline footnote] definition.

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**

    and multiple paragraphs.

[^second]: Footnote text.
```

Footnote 1 link[^first].

Footnote 2 link[^second].

Inline footnote^[Text of inline footnote] definition.

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**

    and multiple paragraphs.

[^second]: Footnote text.


### [Definition lists](https://github.com/markdown-it/markdown-it-deflist)

```
Term 1

:   Definition 1
with lazy continuation.

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

_Compact style:_

Term 1
  ~ Definition 1

Term 2
  ~ Definition 2a
  ~ Definition 2b
```

Term 1

:   Definition 1
with lazy continuation.

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

_Compact style:_

Term 1
  ~ Definition 1

Term 2
  ~ Definition 2a
  ~ Definition 2b


### [Abbreviations](https://github.com/markdown-it/markdown-it-abbr)

```
This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

*[HTML]: Hyper Text Markup Language
```

This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

*[HTML]: Hyper Text Markup Language


### [Custom containers](https://github.com/markdown-it/markdown-it-container)

Supported containers: `warning, caution, danger, important, tip, info, note`.

```
::: warning
**Warning**

*Here be dragons*
:::
```
::: warning
**Warning**

*Here be dragons*
:::

```
::: caution
**Caution**

Be careful!
:::
```

::: caution
**Caution**

Be careful!
:::

```
::: danger
**Danger**

Danger Will Robinson!
:::
```

::: danger
**Danger**

Danger Will Robinson!
:::

```
::: important
**Important**

This is important!
:::
```

::: important
**Important**

This is important!
:::

```
::: tip
**Tip**

This is a tip.
:::
```

::: tip
**Tip**

This is a tip.
:::

```
::: info
**Information**

This supplies some information.
:::
```

::: info
**Information**

This supplies some information.
:::

```
::: note
**Note**

Take note!
:::
```

::: note
**Note**

Take note!
:::


### [Underlined text](https://www.npmjs.com/package/markdown-it-underline)

`_This is underlined text_`

_This is underlined text_

### [Keystroke tags](https://www.npmjs.com/package/markdown-it-kbd)

`[[Ctrl]] + [[B]]` produces the output below.

[[Ctrl]] + [[B]]

### [Checkboxes](https://www.npmjs.com/package/markdown-it-checkbox)

`[ ]` Unchecked

[ ] Unchecked

`[x]` Checked

[x] Checked


### [MultiMarkdown Table](https://www.npmjs.com/package/markdown-it-multimd-table)

Add more control over the formatting of tables.

**Markdown**

```
|             |          Grouping           ||
First Header  | Second Header | Third Header |
 ------------ | :-----------: | -----------: |
Content       |          *Long Cell*        ||
Content       |   **Cell**    |         Cell |                                         
New section   |     More      |         Data |
And more      | With an escaped '\|'       ||
[Prototype table]                              
```

**Output**

|             |          Grouping           ||
First Header  | Second Header | Third Header |
 ------------ | :-----------: | -----------: |
Content       |          *Long Cell*        ||
Content       |   **Cell**    |         Cell |                                         
New section   |     More      |         Data |
And more      | With an escaped '\|'       ||
[Prototype table]             

Lists and sentences over multiple lines.

**Markdown**
```

First header | Second header
-------------|---------------
List:        | More  \
- over       | data  \
- several    |       \
- lines      |
```

**Output**

First header | Second header
-------------|---------------
List:        | More  \
- over       | data  \
- several    |       \
- lines      |

### [Decorate - add attributes, IDs and classes](https://www.npmjs.com/package/markdown-it-decorate)

Gives more control to how you display your content. 

For example, if you assign a *small* class to an image, you can then set the style in your stylesheet, so that all images with `class="small"` have a maximum width of 200 pixels.

**Note:** You will need to add the required class styling to your stylesheet for decorations to work.

**Markdown**

`Red text <!--{.red}-->`

**Output**

Red text <!--{.red}-->

**Style**

``` html
.red{
    color: Red;
}
```

**Markdown**

`![Minion](https://octodex.github.com/images/minion.png) <!--{.small}-->`

**Output**

![Minion](https://octodex.github.com/images/minion.png) <!--{.small}-->

**Style**

``` html
img.small{
    max-width: 200px;
}
```


