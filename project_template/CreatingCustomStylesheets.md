include::(includes/header.md)

include::(includes/navigation.md)

# Creating Custom stylesheets

## Make a custom stylesheet

To make your own custom stylesheet for use with MarkdownDox:

1. Copy an existing stylesheet from the stylesheet folder (`/css`) of a MarkdownDox project.
2. Modify the stylesheet as desired and save it to your project's stylesheet folder.
3. Set the `stylesheet_path` value in the `mdox_config.json` file to your custom stylesheet and save the configuration file.
4. Generate a HTML file with your own custom styling.
5. Modify your custom stylesheet and re-generate the HTML file.
6. Repeat the process until you are happy with your custom stylesheet.

Use the information below to help create your own custom stylesheet.

### How HTML and PDF files get their styling

MarkdownDox HTML and PDF output files get their look and feel from a Cascading Stylesheet (CSS) file linked to (`<link>`) from within the generated HTML source code or from CSS styles included in the `<style>...</style> ` element within each HTML file.

The stylesheet that MarkdownDox uses to read style information from is configured in each project's configuration file - `mdox_config.json` in the `stylesheet_path` configuration option. 

MarkdownDox comes with a built-in set of stylesheet files:

- mdox_default.css
- mdox_official.css
- mdox_simple.css
- mdox_simpe-sans.css
- mdox_writer.css.

If the built-in stylesheet files do not suit the purposes of your documentation, you can create custom stylesheets of your own.

### MarkdownDox's generated HTML layout

MarkdownDox aims to provide a very simple static HTML and PDF generation solution. Therefore it does not use a templating system, such as [Handlebars](https://handlebarsjs.com/) or [Mustache](https://mustache.github.io/) to define the layout of its HTML elements. 

MarkdownDox generates HTML with the following fixed layout of elements, based on [HTML5's semantic HTML layout](https://www.w3schools.com/html/html5_semantic_elements.asp):

```
<html>
    <body>
        <header></header>
        <section class="container">
            <nav class="project_toc"></nav>
            <section class="content"></section>
        </section>
        <footer></footer>
    </body>
</html>
```

![HTML layout](images/HTMLLayout.png)

This fixed layout makes it relatively easy to modify one of the existing MarkdownDox style sheets or to create a new one from scratch.

### Styling HTML elements and classes

The elements and classes below are used in MarkdownDox's HTML layout and can have their styling defined as desired. Use the existing MarkdownDox stylesheets as a starting point for your customisations.

Elements specific to MarkdownDox's output HTML layout include:

```
//The header content read from the `header.md` include file.

header {
    property: value;
}

//The footer content read from the `footer.md` include file.

footer{
    property: value;
}

section {
    property: value;
}

nav{
    property: value;
}
```

Classes specific to MarkdownDox's output HTML layout include:

```
//Contains the sidebar and content.

.container{
    property: value;
}

//The project level Table of Contents read from the `navigation.md` include file

.project_toc{
    property: value;
}

//Applied to the (<li>) list element which is the link to the current file in the project Table of Contents (project_toc). This is highlighted in the default stylesheets/

.selected_nav_item{
    background-color: darkgrey;
    padding: 5px;
    margin: 2px 5px 2px 0;
    border-radius: 3px;
}

//The main content section generated from the Markdown file.

.content{
    property: value;
}

//To insert page breaks into the generated PDF file

.page {
  page-break-before: always;
  margin-top: 40px;
}

//Admonitions

.warning{
    property: value;
}

.caution{
    property: value;
}

.danger{
    property: value;
}

.important{
    property: value;
}

.tip{
    property: value;
}

.info{
    property: value;
}

.note {
    property: value;
}
```


include::(includes/footer.md)