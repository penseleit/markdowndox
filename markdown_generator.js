var vscode = require('vscode');
var config_worker = require('./config_worker');
var fs = require('fs');
var path = require('path');

module.exports = {

    // Create a single Markdown file from all Markdown files in project - for use in creating a single HTML file and then a PDF file from that
    createSingleMarkdownFile: function(fileUri) {
        
        if (! fileUri){
            return;
        }
        
        // Get file list from project configuration - this determines the order that the files should be joined
        var projectConfig = config_worker.getConfigJSON(fileUri);    
        var rootFolder = config_worker.getRootFolder(fileUri);

        if(! rootFolder){
            console.error('::markdown_generator:: Error setting root folder path.');
            vscode.window.showErrorMessage('Error setting root folder path.');
            return;
        }
        var outputMDFileName = projectConfig.project_name;
    
        if (! outputMDFileName){
            console.error('::markdown_generator:: Error reading "project_name" from configuration file.');
            vscode.window.showErrorMessage('Error reading "project_name" from configuration file.');
            return;
        }
    
        outputMDFileName = outputMDFileName.replace(/\s+/g, '') + '.md'; //Remove whitespace from filename
        
        if(projectConfig.pdfProjectFilesList && projectConfig.pdfProjectFilesList.length > 0) {

            var singleMDFileContents = "";
            var missingFiles = [];

            //Open each file listed in the project configuration and add contents to single string variable
            for (var i = 0; i < projectConfig.pdfProjectFilesList.length; i++){
            
            
                var mdFilePath = path.join(rootFolder, path.sep, projectConfig.pdfProjectFilesList[i]);
                var mdFileContents = "";

                try {
                    //Check file exists and is readable
                    fs.accessSync(mdFilePath, fs.constants.F_OK | fs.constants.R_OK);

                    mdFileContents = fs.readFileSync(mdFilePath, 'utf8');
                }
                catch(e){
                    console.error('::markdown_generator:: Error opening Markdown file "' + path.basename(mdFilePath) + '". This file\'s contents have not been written to the output file. ' + e.message);     
                    missingFiles.push(projectConfig.pdfProjectFilesList[i]); 
                    //Don't return - just keep iterating through file names
                }
            
                if(mdFileContents){

                    //Add anchor for TOC navigation
                    singleMDFileContents += '\r\n<div class="single_md" id="' + projectConfig.pdfProjectFilesList[i].substring(0, projectConfig.pdfProjectFilesList[i].indexOf('.')) +'" />\r\n\r\n';
                
                    singleMDFileContents += mdFileContents + '\r\n\r\n'; //Add new lines

                    if (projectConfig.insert_page_breaks){
                        singleMDFileContents += '<div class="page" />' + '\r\n\r\n'; //Add page break and new lines
                    }

   }
            }
            
            // Write out new single Markdown file     
            try{
                
                //Write single Markdown file to output
                fs.writeFileSync(path.join(rootFolder, path.sep, outputMDFileName),singleMDFileContents);
            }
            catch(e){
                console.error('::markdown_generator:: Error writing Markdown file: "' + outputMDFileName + '". This file\'s contents have not been written to the output file. \r\n' + e.message);
                return;     
            }

            if(missingFiles.length > 0)
            {
                vscode.window.showWarningMessage('File "' + outputMDFileName + '" has been created from project Markdown files. \r\n\r\n However, the contents of the following files were unable to be included: ' + missingFiles.join().replace(/,/g, ', '));
            }
            else{

                vscode.window.showInformationMessage('File "' + outputMDFileName + '" has been created from project Markdown files.');
            }
            
        }
        else {
            console.error('::markdown_generator:: The array list of project files to process into a single Markdown file ("pdfProjectFilesList") is not present in the project configuration file.');
            vscode.window.showErrorMessage('The array list of project files to process into a single Markdown file ("pdfProjectFilesList") is not present in the project configuration file.');
            return;
        }
    
        var singleFileURI = vscode.Uri.file(path.join(rootFolder, path.sep, outputMDFileName));

        return singleFileURI;
    } 
};