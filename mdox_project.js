var vscode = require('vscode');
var config_worker = require('./config_worker');
var fs = require('fs');
var path = require('path');
var utils = require('./utils.js');

module.exports = {
    createMDoxProject: function () {

        //Select folder to create new project in
        const options = vscode.OpenDialogOptions = {
            canSelectFiles: false,
            canSelectFolders: true,
            canSelectMany: false,
            openLabel: 'Choose folder for new project',
            filters: { }
        };

        vscode.window.showOpenDialog(options).then(selectedFileUri => {
            if (selectedFileUri && selectedFileUri[0]) {

                try {
                    
                    var myExtDir = vscode.extensions.getExtension('penseleit.markdowndox').extensionPath;
                    var configFileName = '/mdoxconfig.json';
                    var projectConfig = config_worker.getDefaultConfigurationObject(); 

                    //Check for existing files in folder
                    var filesInFolder = fs.readdirSync(selectedFileUri[0].fsPath.toString());
                    if (filesInFolder && filesInFolder.length > 0){
                        console.error('::mdox_project:: There are existing files in the selected folder. The \'create new project\' operation cannot proceed. Please choose an empty folder.');

                        vscode.window.showErrorMessage('There are existing files in the selected folder. The \'create new project\' operation cannot proceed. Please choose an empty folder.');
                        
                        return;
                    }

                    //Copy project template
                    utils.copyFolderAndContentToOutput(path.join(myExtDir, path.sep, '/project_template'), selectedFileUri[0].fsPath);  
                    
                    //Create new 'images' folder
                    if(!fs.existsSync(path.join(selectedFileUri[0].fsPath, path.sep, projectConfig.images_folder))){
                        fs.emptyDirSync(path.join(selectedFileUri[0].fsPath, path.sep, projectConfig.images_folder));
                    }
                    
                    //Create new output folder
                    if(!fs.existsSync(path.join(selectedFileUri[0].fsPath, path.sep, projectConfig.project_output_folder))){
                        fs.mkdirSync(path.join(selectedFileUri[0].fsPath, path.sep, projectConfig.project_output_folder));
                    }
                    
                    //Write default config file
                    var jsonOutPath = path.join(selectedFileUri[0].fsPath, path.sep, configFileName);
                    var mdoxconfig_defaults = config_worker.getDefaultConfigurationJSON();
                    fs.writeFileSync(jsonOutPath, mdoxconfig_defaults); //Create default project JSON configuration file

                    console.log('New MarkdownDox project created successfully in folder: '+ selectedFileUri[0].fsPath);
                    vscode.window.showInformationMessage('New MarkdownDox project created successfully in folder: '+ selectedFileUri[0].fsPath);
                }
                catch(e) {
                    console.error('::mdox_project:: Error while creating new MarkdownDox project: ' + e.message + '\r\n' + e);
                    vscode.window.showErrorMessage('Error while creating new MarkdownDox project: ' + e.message,);
                    return;
                }
            }
            
        })
        return;
    }
};