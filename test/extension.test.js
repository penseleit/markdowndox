/* global suite, test */

//
// Note: This example test is leveraging the Mocha test framework.
// Please refer to their documentation on https://mochajs.org/ for help.
//

// The module 'assert' provides assertion methods from node
const assert = require('assert').strict;

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
// const vscode = require('vscode');
// const myExtension = require('../extension');
const vscode = require('vscode');
const path = require('path');
const config_worker = require('../config_worker');

// Defines a Mocha test suite to group tests of similar kind together
suite("Extension Tests", function() {

    // Defines a Mocha unit test
    test("Default configuration string test", () => {
        let defaultConfig = `{
         "project_name": "My MarkdownDox Project",
         "images_folder": "./images",
         "stylesheet_folder": "./css",
         "includes_folder": "./includes",
         "project_output_folder": "./docs",
         "stylesheet_path": "./css/mdox_default.css",
         "highlight_stylesheet_path": "./css/default.css",
         "use_internal_stylesheet": false,
         "insert_page_breaks": true,
         "project_files_list": [
             "index.md",
             "GettingStarted.md",
             "Sample.md",
             "CreatingCustomStylesheets.md"    
         ],
         "show_errors_in_output_html": true,
         "auto_include_header_footer_nav": true,
         "auto_generate_html_on_file_change": true,
         "auto_generate_html_on_include_file_change": true,
         "markdown_it_toc_settings":{
             "includeLevel": [2, 3, 4],
             "containerClass": "table-of-contents",
             "slugify": "(s) => encodeURIComponent(String(s).trim().toLowerCase().replace(/\s+/g, '-'))",
             "markerPattern": "/^\\[\\[toc\\]\\]/im",
             "listType": "ul",
             "format": "",
             "forceFullToc": false,
             "containerHeaderHtml": "",
             "containerFooterHtml": ""
         },
         "markdown_it_anchor_settings": {
             "level": 1,
             "slugify": "",
             "permalink": false,
             "renderPermalink": "",
             "permalinkClass": "header-anchor",
             "permalinkSymbol": "¶",
             "permalinkBefore": false,
             "permalinkHref": "",
             "callback": ""
         },
         "markdown_it_checkbox_settings": {
         "divWrap": true,
         "divClass": "cb",
         "idPrefix": "cbx_"
         },
         "markdown_it_emoji_settings": {
         "defs": "",
         "enabled": "",
         "shortcuts": ""
         },
         "pdfChromeExecutablePath": "",
         "pdfProjectFilesList": [
         "index.md",
         "GettingStarted.md",
         "Sample.md",
         "CreatingCustomStylesheets.md"    
         ],
         "pdfScale": 1,
         "pdfPaperFormat": "A4",
         "pdfDisplayHeaderFooter": true,
         "pdfHeaderTemplate": " ",
         "pdfFooterTemplate": "<div style=\"width: 100%; font-family: Arial; font-size: 9px; margin 0 auto; text-align: center \"><span style=\"margin-left: 20px;\">Copyright &copy; 2019 My Company</span> Page  <span class='pageNumber'></span> of <span class='totalPages'></span></div>",
         "pdfPrintBackgroundGraphics": true,
         "pdfOrientationLandscape": false,
         "pdfPageRanges": "",
         "pdfWidth": "",
         "pdfHeight": "",
         "pdfMarginTop": "1cm",
         "pdfMarginRight": "",
         "pdfMarginBottom": "1cm",
         "pdfMarginLeft": "",
         "pdfPreferCSSPageSize": false
}`
        

        let config = config_worker.getDefaultConfigurationJSON();
        assert(config != null && config != undefined && typeof(config) == 'string' && config.localeCompare(defaultConfig), 'Default configuration string test failed.');
    })

    test('Default configuration object test', function() {
        let defaultConfigObject = config_worker.getDefaultConfigurationObject();
        assert(defaultConfigObject != null && defaultConfigObject != undefined && typeof(defaultConfigObject) == 'object',  'Default configuration object test failed.');
    });

    test('Get configuration - handle incorrect type passed into function', function() {
        let testFileUri = vscode.window.activeTextEditor.document.uri;
        let configJSON = config_worker.getConfigJSON(testFileUri.fsPath);
        assert.strictEqual(configJSON, '', 'Get configuration - handle incorrect type passed into function - test failed!');
    });

    test('Get configuration - handle no file', function() {
        let badTestFileUri = vscode.Uri.file('C:/red/red.json');
        let configJSON = config_worker.getConfigJSON(badTestFileUri);
        assert.strictEqual(configJSON, '', 'Get configuration - handle no file - test failed!');
    });

    test('Get configuration', function() {
        let testFileUri = vscode.window.activeTextEditor.document.uri;
        let configJSON = config_worker.getConfigJSON(testFileUri);
        assert(typeof(testFileUri) == 'object' && testFileUri.scheme == 'file', 'Get configuration - Uri not used to pass into function');
        assert(configJSON != null && configJSON != undefined && configJSON != '' && typeof(configJSON) == 'object', 'Get configuration - test failed!');
    });


    test('Get root folder - handle no file', function() {

        let rootFolder = config_worker.getRootFolder(null);
        assert.strictEqual(rootFolder, '', 'Get root folder - handle no file - test failed');
    });

    test('Get root folder', function() {

        let testFileUri = vscode.window.activeTextEditor.document.uri;
        let rootFolder = config_worker.getRootFolder(testFileUri);
        assert.strictEqual(rootFolder, path.dirname(vscode.window.activeTextEditor.document.uri.fsPath), 'Get root folder test failed');
    });


    test('Make single MD file', function() {

        let singleMDUri = md_generator.createSingleMarkdownFile(vscode.window.activeTextEditor.document.uri);
        assert(singleMDUri != null && singleMDUri != undefined && singleMDUri != '' && singleMDUri.scheme == 'file' && path.basename(singleMDUri.fsPath) == 'MyMarkdownDoxProject.md', 'Make single MD file test failed');
    });


});