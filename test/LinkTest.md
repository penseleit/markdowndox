# Test of internal and external links

## Internal links

- [PDF Options](index.html#PDFOptions)
- [Getting Started](GettingStarted.html)
- [Porrideg](Porridge.html)
- [Link 1 CCS](CreatingCustomStylesheets.html)
- [Link 2 Index](index.html)
- [# PDF Options](#PDFOptions)
- [Getting Started](GettingStarted)
- [CCS](@CreatingCustomStylesheets.md)
- [Index](index#)
- [#I#PDF Options](#index#PDFOptions)
- [Getting Started](GettingStarty.html)



## External Links

- [Google](http://www.google.com)
- [BorderNet](https://bordernet.immi.local/)
- [Oracle](https://www.oracle.com/au/index.html)
- [JQuery](https://learn.jquery.com/using-jquery-core/avoid-conflicts-other-libraries/#including-jquery-before-other-libraries)
- [Joachim Kuehn](https://www1.wdr.de/radio/wdr5/sendungen/tischgespraech/joachim-kuehn-108.html)
- [Fixed Gear Gallery](http://fixedgeargallery.com/2019/01/24/breakbrake17/)
- [Rule #5](http://www.velominati.com/#credit-2)
- [CircleCI](https://circleci.com/docs/2.0/glossary/#container)

