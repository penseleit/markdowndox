var vscode = require('vscode');
var config_worker = require('./config_worker');
var fs = require('fs-extra'); //Adds support for copying folders
var path = require('path');
var hljs = require('highlight.js'); // https://highlightjs.org/
var utils = require('./utils.js');

var generator = null;

function generatorInit(fileUri){

 
    //Init Markdown-It and extended features
    var mdTOCOptions = null;
    var emojiOptions = null;
    var anchorOptions = null;
    var checkboxOptions = null;

    var config = config_worker.getConfigJSON(fileUri);
    if(config){
        mdTOCOptions = config.markdown_it_toc_settings;
        emojiOptions = config.markdown_it_emoji_settings;
        anchorOptions = config.markdown_it_anchor_settings;
        checkboxOptions = config.markdown_it_checkbox_settings;
    }

    generator = require('markdown-it')({
            html:         true,        
            xhtmlOut:     false,        
            breaks:       false,        
            linkify:      true,        
            typographer:  true,
            highlight: function (str, lang) {
                if (lang && hljs.getLanguage(lang)) {
                  try {
                    return hljs.highlight(lang, str).value;
                  } catch (__) {}
                }
            
                return ''; // use external default escaping
              }
        })
        .use(require('markdown-it-abbr'))
        .use(require('markdown-it-anchor', anchorOptions))
        .use(require('markdown-it-checkbox', checkboxOptions))
        .use(require('markdown-it-container'), 'note')
        .use(require('markdown-it-container'), 'warning')
        .use(require('markdown-it-container'), 'caution')
        .use(require('markdown-it-container'), 'danger')
        .use(require('markdown-it-container'), 'important')
        .use(require('markdown-it-container'), 'tip')
        .use(require('markdown-it-container'), 'info')
        .use(require('markdown-it-decorate'))
        .use(require('markdown-it-deflist'))
        .use(require('markdown-it-emoji', emojiOptions))
        .use(require('markdown-it-footnote'))
        .use(require('markdown-it-ins'))
        .use(require('markdown-it-kbd'))
        .use(require('markdown-it-mark'))
        .use(require('markdown-it-sup'))
        .use(require('markdown-it-sub'))
        .use(require('markdown-it-table-of-contents', mdTOCOptions ))
        .use(require('markdown-it-underline'))
        .use(require('markdown-it-multimd-table'), {enableMultilineRows: true});
       

        
};

function makeMDContent(mdPath, mdFolder, includesFolder, showErrorsInHTML, autoIncludeHdrFtrNav){

    try{
       
        // Read Markdown content file
        var mdContent = fs.readFileSync(mdPath,'utf8'); 

        //Process include files
        var processedMDContent = processIncludes(mdContent, mdFolder, includesFolder, showErrorsInHTML, autoIncludeHdrFtrNav);

        return processedMDContent;
    }
    catch(e){
        console.error('::html_generator:: Error assembling Markdown from components. ' + e.message);
        return '';
    }
}

function processIncludes(markdown, folderPath, includesFolder, showErrorsInHTML, autoIncludeHdrFtrNav){

    var includeRegEx = /include::\([A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*\)\s*$/igmu;
    
    if (includeRegEx.test(markdown)){ //Are there any include tokens in the content?
        
        includeRegEx.lastIndex = 0;
        var includeArray = [];
        var incPathContentsArray = [];
        var regexArray;
        var footerContent = '';    
        var headerContent = ''     
        var asideContent = '';     
        var navContent = '';       

        while((regexArray = includeRegEx.exec(markdown)) !== null){
            if(! includeArray.includes(regexArray[0].trim())){ // Don't add duplicates
                includeArray.push(regexArray[0].trim());
            }
        }
        
        //Create array of include tokens and their corresponding file contents
        for (var i = 0; i < includeArray.length; i++){
            
            var includeToken = includeArray[i].trim();
            var incPath = path.normalize(includeToken.substring(includeToken.indexOf('(') + 1, includeToken.length - 1));
            incPath = path.join(folderPath, path.sep, incPath);
                   
            if (fs.existsSync(incPath)){
            
                try{
                    fs.accessSync(incPath, fs.constants.F_OK | fs.constants.R_OK);

                    var incContents = fs.readFileSync(incPath,'utf8');

                    if (incContents && incContents.length > 0){
                        
                        if(path.basename(incPath) == 'navigation.md'){
                            navContent += incContents;
                            incPathContentsArray.push([includeArray[i], ' ']); //This will replace the include statement with whitespace
                            continue;
                        }

                        if(path.basename(incPath) == 'header.md'){
                            headerContent += incContents;
                            incPathContentsArray.push([includeArray[i], ' ']);
                            continue;
                        }

                        if(path.basename(incPath) == 'footer.md'){
                            footerContent += incContents;
                            incPathContentsArray.push([includeArray[i], ' ']);
                            continue;
                        }

                        if(path.basename(incPath) == 'sidebar.md'){
                            asideContent += incContents;
                            incPathContentsArray.push([includeArray[i], ' ']);
                            continue;
                        }

                        incPathContentsArray.push([includeArray[i], incContents]);
                    }
                    else{
                        incPathContentsArray.push([includeArray[i], showErrorsInHTML ? 'INCLUDE FILE CONTENTS NOT FOUND' : ' ']); //So that the include token is replaced with a message in the output html
                    }
                                    
                }
                catch(e){
                    console.error('::html_generator:: Error processing include file: "' + incPath + '". ' + e.message);
                    incPathContentsArray.push([includeArray[i], showErrorsInHTML ? 'INCLUDE FILE CONTENTS NOT FOUND' : ' ']); //So that the include token is replaced with a message in the output html    
                }
            }
            else{
                incPathContentsArray.push([includeArray[i], showErrorsInHTML ? 'INCLUDE FILE CONTENTS NOT FOUND' : ' ']); //So that the include token is replaced with a message in the output html
            }
        }

        //Replace include token with file contents in source markdown
        for (var m = 0; m < incPathContentsArray.length; m++){
            
            var leftBracket = /[(]/g;
            var rightBracket = /[)]/g;
            var incTokenString = incPathContentsArray[m][0].trim();
            
            incTokenString = incTokenString.replace(leftBracket,'\\(');
            incTokenString = incTokenString.replace(rightBracket,'\\)');
            var incRegex = new RegExp(incTokenString, 'igmu');
            
            if(incRegex.test(markdown)){
                incRegex.lastIndex = 0;
                markdown = markdown.replace(incRegex, incPathContentsArray[m][1]);
            }
        }
    }

    //Populate header footer and nav even if not an include in the markdown
    if(autoIncludeHdrFtrNav){

        if(! headerContent){
            var headerPath = path.join(folderPath, path.sep, includesFolder, path.sep, 'header.md');
                   
            if (fs.existsSync(headerPath)){
            
                try{
                    fs.accessSync(headerPath, fs.constants.F_OK | fs.constants.R_OK);

                    var incContents = fs.readFileSync(headerPath,'utf8');

                    if (incContents && incContents.length > 0){
                        headerContent = incContents;
                    }
                }
                catch(e){
                    console.error('::html_generator:: Error processing header include file: ' + e.message);
                }
            }
        }

        if (! footerContent){
            var footerPath = path.join(folderPath, path.sep, includesFolder, path.sep, 'footer.md');
                   
            if (fs.existsSync(footerPath)){
            
                try{
                    fs.accessSync(footerPath, fs.constants.F_OK | fs.constants.R_OK);

                    var incContents = fs.readFileSync(footerPath,'utf8');

                    if (incContents && incContents.length > 0){
                        footerContent = incContents;
                    }
                }
                catch(e){
                    console.error('::html_generator:: Error processing footer include file: ' + e.message);
                }
            }
        }

        if (! navContent){
            var navPath = path.join(folderPath, path.sep, includesFolder, path.sep, 'navigation.md');
                   
            if (fs.existsSync(navPath)){
            
                try{
                    fs.accessSync(navPath, fs.constants.F_OK | fs.constants.R_OK);

                    var incContents = fs.readFileSync(navPath,'utf8');

                    if (incContents && incContents.length > 0){
                        navContent = incContents;
                    }
                }
                catch(e){
                    console.error('::html_generator:: Error processing navigation include file: ' + e.message);
                }
            }
        }
    }

    var markdownContent = {
        "header": headerContent,
        "footer": footerContent,
        "nav": navContent,
        "aside": asideContent,
        "content": markdown
    }

    return markdownContent;
}


function markSelectedInNav(htmlContent, filename){

    var index = htmlContent.indexOf(filename);
    if (index && index > 0){
        var navIndex = htmlContent.lastIndexOf('<a href=',index);
        var pre = htmlContent.substring(0, navIndex);
        var post = htmlContent.substring(navIndex + 3);
        htmlContent = pre + '<a class="selected_nav_item" ' + post;

    }


    return htmlContent;
}


function changeNavToLinkToInternalBookmarks(nav, section){
 
    //Find existing internal anchors and remove filename
    var internalAnchorRegEx  = /<\s*a\s*href=\s*"*[A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*.html#[A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*\s*"\s*>/igmu;
    var internalRegexArray;
    var internalAnchorArray = [];

     while((internalRegexArray = internalAnchorRegEx.exec(nav)) !== null){
        internalAnchorArray.push(internalRegexArray[0].trim());
    }


    if (internalAnchorArray && internalAnchorArray.length > 0){    
        for (var x = 0; x < internalAnchorArray.length; x++){
            if(internalAnchorArray[x] != ''){
                var newInternalAnchor = internalAnchorArray[x].split('#')[1];
                var indexB = newInternalAnchor.indexOf('"');
                newInternalAnchor = '#' + newInternalAnchor.substring(0, indexB);
                var oldInternalAnchor =  internalAnchorArray[x].split('href="')[1];
                var indexC = oldInternalAnchor.lastIndexOf('"');
                oldInternalAnchor = oldInternalAnchor.substring(0, indexC);
                var htmlAnchor = internalAnchorArray[x].split('href="')[1];
                var indexD = htmlAnchor.indexOf('.html');
                htmlAnchor = htmlAnchor.substring(0,indexD);

                if(section.indexOf('<div class="single_md" id="' + htmlAnchor + '" />') != -1){
                    nav = nav.replace(oldInternalAnchor, newInternalAnchor);
                }
            }
        }
    }

    //Find html links and convert to internal anchors
    var anchorRegEx  = /<\s*a\s*href=\s*"*[A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*.html\s*"\s*>/igmu;
    var regexArray;
    var anchorArray = [];

     while((regexArray = anchorRegEx.exec(nav)) !== null){
        anchorArray.push(regexArray[0].trim());
    }

    nav.replace('class="selected_nav_item"', "");

    if (anchorArray && anchorArray.length > 0){    
        for (var x = 0; x < anchorArray.length; x++){
            if(anchorArray[x] != ''){
                var newAnchor = anchorArray[x].split('href="')[1];
                var indexA = newAnchor.indexOf('.html');
                var oldAnchor = newAnchor.substring(0, indexA + 5);
                newAnchor = newAnchor.substring(0,indexA);

                if(section.indexOf('<div class="single_md" id="' + newAnchor + '" />') != -1){
                    newAnchor = '#' + newAnchor;
                    nav = nav.replace(oldAnchor, newAnchor);
                }
            }
        }
    }


    return nav;
}

function changeLinksToInternalBookmarks(nav, htmlContent){

       //Find existing internal anchors and remove filename
       var internalAnchorRegEx  = /<\s*a\s*href=\s*"*[A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*.html#[A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*\s*"\s*>/igmu;
       var internalRegexArray;
       var internalAnchorArray = [];
       var newHTMLContent = htmlContent;
       
       while((internalRegexArray = internalAnchorRegEx.exec(newHTMLContent)) !== null){
           internalAnchorArray.push(internalRegexArray[0].trim());
       }
   
   
       if (internalAnchorArray && internalAnchorArray.length > 0){    
           for (var x = 0; x < internalAnchorArray.length; x++){
               if(internalAnchorArray[x] != ''){
                   var newInternalAnchor = internalAnchorArray[x].split('#')[1];
                   
                   var indexB = newInternalAnchor.indexOf('"');
                   newInternalAnchor = '#' + newInternalAnchor.substring(0, indexB);
                   var oldInternalAnchor =  internalAnchorArray[x].split('href="')[1];
                   var indexC = oldInternalAnchor.lastIndexOf('"');
                   oldInternalAnchor = oldInternalAnchor.substring(0, indexC);
                   var htmlAnchor = internalAnchorArray[x].split('href="')[1];
                   var indexD = htmlAnchor.indexOf('.html');
                   htmlAnchor = htmlAnchor.substring(0,indexD + 5);
                   
                   if(nav.indexOf('href="' + htmlAnchor + '"') != -1 || nav.indexOf('href="' + newInternalAnchor + '"') != -1){
                        newHTMLContent = newHTMLContent.replace(new RegExp(oldInternalAnchor, 'igmu'), newInternalAnchor);
                   }
                   
               }
           }
       }
   
       //Find html links and convert to internal anchors
       var anchorRegEx  = /<\s*a\s*href=\s*"*[A-Za-z0-9-._~:\/?#\[\]@!$&'()*+,;=]*.html\s*"\s*>/igmu;
       var regexArray;
       var anchorArray = [];
   
        while((regexArray = anchorRegEx.exec(newHTMLContent)) !== null){
           anchorArray.push(regexArray[0].trim());
       }
   
       if (anchorArray && anchorArray.length > 0){    
           for (var x = 0; x < anchorArray.length; x++){
               if(anchorArray[x] != ''){
                    var newAnchor = anchorArray[x].split('href="')[1];
                    var oldPrefix = anchorArray[x].split('href="')[0];
                    var indexA = newAnchor.indexOf('.html');
                    var oldAnchor = newAnchor.substring(0, indexA + 5);
                    var newAnchorPart = newAnchor.substring(0,indexA);
                    newAnchor = newAnchorPart;
                    newAnchorPart = '#' + newAnchorPart;

                    newAnchor = oldPrefix + 'href="#' + newAnchor + '">';
                    if(nav.indexOf('href="' + oldAnchor + '"') != -1 || nav.indexOf('href="' + newAnchorPart + '"') != -1){
                        newHTMLContent = newHTMLContent.replace(new RegExp(anchorArray[x],'igmu'), newAnchor);  
                    }  
               }
           }
       }
   
   
       return newHTMLContent;
}

function  createFile (fileUri, rootFolder, outputFileName, config) {
    try{
        // Process MD file into HTML
        if (! fileUri){
            return;
        }

        var htmlTitle = path.basename(fileUri.fsPath);
        if(htmlTitle){
            htmlTitle.trim();
            htmlTitle = htmlTitle.substring(0,htmlTitle.indexOf('.'));
            htmlTitle = htmlTitle.substring(0,1).toUpperCase() + htmlTitle.substring(1);
        }

        //Check file exists and is readable
        fs.accessSync(fileUri.fsPath, fs.constants.F_OK | fs.constants.R_OK);

        var cssString = "";

        if(config.use_internal_stylesheet){
            var cssFileContents = fs.readFileSync(path.join(rootFolder, path.sep, config.stylesheet_path),'utf8');
            cssString = '<style>\r\n' + cssFileContents + '\r\n</style>';
        }
        else{
            cssString = '<link rel="stylesheet" type="text/css" href="' + path.posix.normalize(config.stylesheet_path) + '" /> \r\n <link rel="stylesheet" type="text/css" href="' + path.posix.normalize(config.highlight_stylesheet_path) + '" />' ;
        }

        // Get Markdown content
        var mdContent = makeMDContent(path.normalize(fileUri.fsPath), rootFolder, config.includes_folder, config.show_errors_in_output_html, config.auto_include_header_footer_nav);

        // Generate HTML from file content
        if (! generator){
            generatorInit(fileUri);
        }

        console.log('Generating HTML file from: "' + path.basename(fileUri.fsPath) + '".');

        var htmlHeader = ' ';
        if (mdContent.header){
            htmlHeader = generator.render(mdContent.header);
            htmlHeader = '\r\n<header>\r\n' + htmlHeader + '\r\n</header>\r\n';
        }
        
        var htmlFooter = ' ';
        if (mdContent.footer){
            htmlFooter = generator.render(mdContent.footer);
            htmlFooter = '\r\n<footer>\r\n' + htmlFooter + '\r\n</footer>\r\n';
        }
        
        var htmlNav = ' ';
        if (mdContent.nav){
            htmlNav = generator.render(mdContent.nav);
            htmlNav = '\r\n<nav class="project_toc">\r\n' + htmlNav + '\r\n</nav>\r\n';

            //Mark currently selected file in navigation
            htmlNav = markSelectedInNav(htmlNav, outputFileName);

            
        }

        var htmlAside = ' ';
        if (mdContent.aside){
            htmlAside = generator.render(mdContent.aside);
            htmlAside = '\r\n<aside class="sidebar">\r\n' + htmlAside + '\r\n</aside>\r\n';
        }

        var htmlSection = ' ';
        if (mdContent.content){
            htmlSection = generator.render(mdContent.content);
            htmlSection = '\r\n<section class="content">\r\n' + htmlSection + '\r\n</section>\r\n';
        }
        
        //Modify nav links if Markdown is a single file created from all project Markdown files
        if (htmlSection.indexOf('<div class="single_md"') != -1){
            htmlNav = changeNavToLinkToInternalBookmarks(htmlNav, htmlSection);
            

        }      

        // HTML template literal
        var htmlPage = 
`<!DOCTYPE html>
<html lang="en">
<head>
<title>${ htmlTitle }</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

${ cssString }

</head>
<body>

${ htmlHeader }

<section class="container">

${ htmlNav }

${ htmlSection }   

${ htmlAside }

</section>

${ htmlFooter }

</body>
</html>`;
        
        //Change project links to internal links
        if (htmlSection.indexOf('<div class="single_md"') != -1){
            htmlPage = changeLinksToInternalBookmarks(htmlNav, htmlPage);
        }

        // Save to .html file
        fs.writeFileSync(path.join(rootFolder, path.sep, config.project_output_folder, path.sep, outputFileName), htmlPage);
    
    }
    catch(e)
    {
        console.error('::html_generator:: Error generating HTML: ' + e.message);
        return;
    }
    
    console.log('File "' + path.basename(fileUri.fsPath)  + '" has been generated as "' + outputFileName + '".');
    vscode.window.showInformationMessage('File "' + path.basename(fileUri.fsPath)  + '" has been generated as "' + outputFileName + '".');
}

module.exports = {

    createFileHTML: function(fileUri){
       
        var config = config_worker.getConfigJSON(fileUri);   
        var rootFolder = config_worker.getRootFolder(fileUri); 
        var outputFileName = path.basename(fileUri.fsPath, '.md') + '.html';
    
        //Create HTML file
        createFile(fileUri, rootFolder, outputFileName, config);
    
     //Copy css to output
     utils.copyFolderAndContentToOutput(path.join(rootFolder, path.sep, config.stylesheet_folder), path.join(rootFolder, path.sep, config.project_output_folder, path.sep, config.stylesheet_folder));
                           
     //Copy images folder to output
     utils.copyFolderAndContentToOutput(path.join(rootFolder, path.sep, config.images_folder), path.join(rootFolder, path.sep, config.project_output_folder, path.sep, config.images_folder));

     var outFileUri = vscode.Uri.file(path.join(rootFolder, path.sep, config.project_output_folder, path.sep, outputFileName));

     return outFileUri;

    } ,

    //Create HTML from all markdown files in project
    createProjectHTML: function(fileUri) {
        
        if (! fileUri){
            return;
        }
            
        var config = config_worker.getConfigJSON(fileUri);    
        var rootFolder = config_worker.getRootFolder(fileUri);

        if(! rootFolder){
            return;
        }
        
        if(config.project_files_list && config.project_files_list.length > 0) {
            
            var missingFiles = [];
            
            //Open each file listed in the project configuration and add contents to single string variable
            for (var i = 0; i < config.project_files_list.length; i++){
            
                var mdFilePath = path.join(rootFolder, path.sep, config.project_files_list[i]);
            
                try {

                    //For each project file create HTML and output to file
                    if(mdFilePath && mdFilePath.length > 0){
    
                        //Check file exists and is readable
                        fs.accessSync(mdFilePath, fs.constants.F_OK | fs.constants.R_OK);
    
                        var mdURI = vscode.Uri.file(mdFilePath);
                        var outputFileName = path.basename(mdURI.fsPath, '.md') + '.html';
                        createFile(mdURI, rootFolder, outputFileName, config);
                    }
                    
                }
                catch(e){
                    console.error('::html_generator:: Error opening Markdown file "' + path.basename(mdFilePath) + '". This file\'s contents have not been generated as HTML. \r\n' + e.message);     
                    missingFiles.push(config.project_files_list[i]); 
                    //Don't return - just keep iterating through file names
                }
            }

            //Copy css to output
            utils.copyFolderAndContentToOutput(path.join(rootFolder, path.sep, config.stylesheet_folder), path.join(rootFolder, path.sep, config.project_output_folder, path.sep, config.stylesheet_folder));
                           
            //Copy images folder to output
            utils.copyFolderAndContentToOutput(path.join(rootFolder, path.sep, config.images_folder), path.join(rootFolder, path.sep, config.project_output_folder, path.sep, config.images_folder));

            if(missingFiles.length > 0)
            {
                console.warn('Some project files have been generated as HTML files.  \r\n\r\n However, the contents of the following files were unable to be processed: ' + missingFiles.join().replace(/,/g, ', '));
                vscode.window.showWarningMessage('Some project files have been generated as HTML files.  \r\n\r\n However, the contents of the following files were unable to be processed: ' + missingFiles.join().replace(/,/g, ', '));
            }
            else{
                console.log('All project files have been generated as HTML files.');
                vscode.window.showInformationMessage('All project files have been generated as HTML files.');
            }
        }  
    },

    fileChangeEventHandler: function(fileUri){

        if (! fileUri){
            return;
        }

        var config = config_worker.getConfigJSON(fileUri);

        if(config){
            if(config.auto_generate_html_on_file_change || config.auto_generate_html_on_include_file_change){
                var includesFldr = 'includes';
                includesFldr = path.normalize(config.includes_folder);   

                if(path.dirname(fileUri.fsPath).indexOf(includesFldr) != -1){
                    try{
                        if(config.auto_generate_html_on_include_file_change){
                            this.createProjectHTML(fileUri);
                        }
                    }
                    catch(e){
                        console.log('Error generating project HTML from detected file change in file: ' + fileUri.fsPath + ' Error: ' + e);     
                    }
                }
                else{
                    try{
                        if(config.auto_generate_html_on_file_change){
                            this.createFileHTML(fileUri);
                        }
                    }
                    catch(e){
                        console.log('Error generating HTML from detected file change in file: ' + fileUri.fsPath + ' Error: ' + e);     
                    }
                }
            }
        }
    }
};

